import React, {useEffect, useState} from 'react';
import {createStackNavigator} from "@react-navigation/stack";
import {NavigationContainer,} from "@react-navigation/native";
import {Main} from "../../screens/Main/Home";
import {Registration} from "../../screens/auth/registration";
import {Login} from "../../screens/auth/login";
import auth from "@react-native-firebase/auth";

const Stack = createStackNavigator();

export const Navigation = ({}) => {
    const [user, setUser] = useState("");
    const [initializing, setInitializing] = useState(true);

    function onAuthStateChanged(user) {
        setUser(user);
        if (initializing) setInitializing(false);

    }

    useEffect(() => {
        return auth().onAuthStateChanged(onAuthStateChanged);
    }, []);

    if (initializing) return null;
    return (

        <NavigationContainer>
            {!user ?
                <Stack.Navigator initialRouteName={Login}>
                    <Stack.Screen name="login" component={Login} options={{headerShown: false}}/>
                    <Stack.Screen name="Registration" component={Registration} options={{headerShown: false}}/>
                </Stack.Navigator>
                :
                <Stack.Navigator initialRouteName={Main}>
                    <Stack.Screen name="Main" component={Main} options={{headerShown: true}}/>
                </Stack.Navigator>
            }
        </NavigationContainer>
    );
}
