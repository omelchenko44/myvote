import React from "react";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import {Profile} from '../Profile/index';
import {Vote} from '../Vote/index';

const Tab = createBottomTabNavigator();
export const Main = () => {
    return (
        <Tab.Navigator>
            <Tab.Screen name="Profile" component={Profile}/>
            <Tab.Screen name="Vote" component={Vote}/>
        </Tab.Navigator>
    );
};