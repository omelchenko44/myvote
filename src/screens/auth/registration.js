import {Keyboard, Text, TouchableOpacity, TouchableWithoutFeedback, View, StyleSheet} from "react-native";
import React, {useState} from "react";
import {Input} from "../../components/Input";
import Feather from "react-native-vector-icons/Feather";
import auth from '@react-native-firebase/auth';
import {useNavigation} from "@react-navigation/core";
import MyAwesomeAlert from "../../components/MyAwesomeAlert";
import i18n from "i18n-js";
import styled from 'styled-components';

export const Registration = ({}) => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [password2, setPassword2] = useState("");
    const [iconName, setIconName] = useState("eye-off");
    const [secureTextEntry, setSecureTextEntry] = useState(true);
    const [iconName2, setIconName2] = useState("eye-off");
    const [secureTextEntry2, setSecureTextEntry2] = useState(true);
    const [showAlert, setShowAlert] = useState();
    const [errorMessage, setErrorMessage] = useState('');
    const {t} = i18n;
    const navigation = useNavigation();

    const newUser = () => {
        if (password !== password2) {
            onShowAlert(t('incorrectPasswordConfirm'))
        } else {
            return ('');
        }
        auth()
            .createUserWithEmailAndPassword(email, password)
            .then(userCredentials => {
                const user = userCredentials.user;
                console.log(user.email);
            })
            .catch((error) => {
                console.log(error.message)
                if (error.message === '[auth/invalid-email] The email address is badly formatted.') {
                    onShowAlert(t('incorrectEmail'))
                }
            })
    }

    function onShowAlert(message) {
        setShowAlert(true);
        setErrorMessage(message);
    }

    function hideAlert() {
        setShowAlert(false);
    }

    return (
        <View style={styles.topContainer}>
        <TouchableWithoutFeedback
            accessible={false}
            onPress={() => {
                Keyboard.dismiss();
            }}>
            <AppViewHeight>
                <View>
                    <Text style={styles.textMyVote}>MyVote</Text>
                    <Text style={styles.textNewAccount}>New Account</Text>
                </View>

                <View style={styles.inputEmail}>
                    <AppViewWidth>
                        <Input
                            value={email}
                            onValue={setEmail}
                            placeholder={'Email'}
                            maxLength={20}
                        />
                    </AppViewWidth>
                    <AppViewWidth>
                        <Input value={password}
                               onValue={setPassword}
                               placeholder={'Create password'}
                               secureTextEntry={secureTextEntry}/>
                        <TouchableOpacity
                            onPress={() => {
                                setIconName(secureTextEntry === true ? "eye" : "eye-off");
                                setSecureTextEntry(!secureTextEntry);
                            }}
                            style={styles.iconTouch}>
                            <Feather name={iconName} size={25}/>
                        </TouchableOpacity>
                    </AppViewWidth>
                    <AppViewWidth>
                        <Input value={password2}
                               onValue={setPassword2}
                               placeholder={'Repeat the password'}
                               secureTextEntry={secureTextEntry2}/>
                        <TouchableOpacity
                            onPress={() => {
                                setIconName2(secureTextEntry2 === true ? "eye" : "eye-off");
                                setSecureTextEntry2(!secureTextEntry2);
                            }}
                            style={styles.iconTouch}>
                            <Feather name={iconName2} size={25}/>
                        </TouchableOpacity>
                    </AppViewWidth>
                </View>
                <View style={styles.regForm}>
                    <TouchableOpacity
                        style={styles.createAccount}
                        onPress={newUser}>
                        <Text style={styles.textCreateAccount}>Create Account</Text>
                    </TouchableOpacity>
                    <View style={styles.haveOnAccount}>
                        <Text>Already have an account?</Text>
                        <TouchableOpacity
                            onPress={() => {
                                navigation.goBack();
                            }}>
                            <Text style={styles.textGoToLoginScreen}>Go to Log In screen</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </AppViewHeight>
        </TouchableWithoutFeedback>
        <MyAwesomeAlert
            showAlert={showAlert}
            errorMessage={errorMessage}
            hideAlert={hideAlert}
            navigation={navigation}
        />
    </View>
    );
};

const styles = StyleSheet.create({
 topContainer: {
    flex: 1
    },
 createAccount: {
    backgroundColor: 'dodgerblue',
    margin: 10,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10
    },
 textMyVote: {
    fontWeight: 'bold',
    fontSize: 25
    },
 textNewAccount: {
    fontWeight: 'bold',
    fontSize: 25,
    marginTop: 30
    },
 textCreateAccount: {
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold'
    },
 haveOnAccount: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 30,
    marginBottom: 10
    },
 textGoToLoginScreen: {
     color: 'dodgerblue',
     marginLeft: 5
    },
 inputEmail: {
     flex: 2,
     alignItems: 'center',
     justifyContent: 'center'
    },
 iconTouch: {
     position: "absolute",
     right: 15,
     bottom: 20
    },
 regForm: {
    flex: 2,
    justifyContent: 'flex-end'
    }
})

const AppViewHeight = styled(View)`
  height: 100%;
`

const AppViewWidth = styled(View)`
  width: 100%;
`