import {Image, Keyboard, Text, TouchableOpacity, TouchableWithoutFeedback, View, StyleSheet,} from "react-native";
import React, {useState} from "react";
import {Input} from "../../components/Input";
import Feather from "react-native-vector-icons/Feather";
import auth from '@react-native-firebase/auth';
import MyAwesomeAlert from "../../components/MyAwesomeAlert";
import i18n from "i18n-js";
import styled from 'styled-components';

export const Login = ({navigation}) => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [iconName, setIconName] = useState("eye-off");
    const [secureTextEntry, setSecureTextEntry] = useState(true);
    const [showAlert, setShowAlert] = useState();
    const [title, setTitle] = useState('');
    const [errorMessage, setErrorMessage] = useState('');
    const {t} = i18n;

    function onLogin() {
        auth()
            .signInWithEmailAndPassword(email, password)
            .then(() => console.log('User signed!'))
            .catch((error) => {
                console.log(error.message)
                if (error.message === '[auth/wrong-password] The password is invalid or the user does not have a password.') {
                    onShowAlert(t('incorrectPassword'));
                }
            })
    }

    function goToRegistration() {
        navigation.navigate("Registration");
    }

    function hideAlert() {
        setShowAlert(false);
    }

    function onShowAlert(message) {
        setShowAlert(true);
        setErrorMessage(message);
    }

    return (
        <View>
            <TouchableWithoutFeedback
                accessible={false}
                onPress={() => {
                    Keyboard.dismiss();
                }}>
                <AppViewHeight>
                    <View style={styles.textReg}>
                        <Text style={styles.textHeaderMyVote}>MyVote</Text>
                        <Text style={styles.textHeaderLogin}>Log In</Text>
                    </View>

                    <View style={styles.inputEmail}>

                        <AppViewWidth>
                            <Input value={email} onValue={setEmail} placeholder={'Email'}/>
                        </AppViewWidth>
                        <AppViewWidth>
                            <Input value={password}
                                   onValue={setPassword}
                                   placeholder={'Password'}
                                   secureTextEntry={secureTextEntry}/>
                            <TouchableOpacity
                                onPress={() => {
                                    setIconName(secureTextEntry === true ? "eye" : "eye-off");
                                    setSecureTextEntry(!secureTextEntry);
                                }}
                                style={styles.iconTouch}>
                                <Feather name={iconName} size={25}/>
                            </TouchableOpacity>
                        </AppViewWidth>
                    </View>
                    <View style={styles.regForm}>
                        <TouchableOpacity
                            style={styles.signIn}
                            onPress={onLogin}>
                            <Text style={styles.textLogIn}>Log In</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.signInGoogle}
                            onPress={() => {
                            }}>
                            <Image source={require('../../assets/google.png')} style={{height: 50, width: 50}}/>
                            <Text style={styles.textSignInGoogle}>Sign in with Google</Text>
                        </TouchableOpacity>
                        <View style={styles.textDontHaveAccount}>
                            <Text>Don`t have an account</Text>
                            <TouchableOpacity
                                onPress={() => {
                                    goToRegistration();
                                }}>
                                <Text style={styles.textCreatAccount}>Create account</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </AppViewHeight>
            </TouchableWithoutFeedback>
            <MyAwesomeAlert
                showAlert={showAlert}
                title={title}
                errorMessage={errorMessage}
                cancelText={"Cancel"}
                confirmText={t('confirmText')}
                hideAlert={hideAlert}
                navigation={navigation}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    signInGoogle: {
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "white",
        shadowColor: "#000",
        borderRadius: 10,
        shadowOffset: {
            width: 6, height: 6,
        },
        shadowOpacity: 0.2,
        shadowRadius: 2,
        elevation: 5,
        margin: 10,
        height: 50,
    }, signIn: {
        backgroundColor: 'dodgerblue',
        margin: 10,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10
    }, textLogIn: {
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold'
    }, textSignInGoogle: {
        fontSize: 20,
        color: 'black',
        fontWeight: 'bold', marginLeft: 5
    }, textHeaderMyVote: {
        fontWeight: 'bold',
        fontSize: 25
    }, textHeaderLogin: {
        fontWeight: 'bold',
        fontSize: 25
    }, textReg: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center'
    }, textCreatAccount: {
        color: 'dodgerblue',
        marginLeft: 5
    }, textDontHaveAccount: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 30,
        marginBottom: 10
    }, inputEmail: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center'
    }, iconTouch: {
        position: "absolute",
        right: 15, bottom: 20
    }, regForm: {
        flex: 2,
        justifyContent: 'flex-end'
    }
})

const AppViewWidth = styled(View)`
  width: 100%;
`

const AppViewHeight = styled(View)`
  height: 100%;
`
