import React from "react";
export default {

    incorrectPassword: 'Ви ввели не вірний пароль або логін',
    incorrectEmail: 'Неправильна електронна адреса',
    incorrectPasswordCharacters: 'Пароль не повинен містити не менше 6 символів',
    incorrectPasswordConfirm: 'Пароли не совпадют'
}
