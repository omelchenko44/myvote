import React from "react";

export default {
    incorrectPassword: 'You entered the wrong username or password',
    incorrectEmail : 'Incorrect email address',
    incorrectPasswordCharacters: 'Password must not be less than 6 characters',
    incorrectPasswordConfirm: 'Passwords do not match'
}
