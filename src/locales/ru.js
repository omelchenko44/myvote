import React from "react";
export default {

    incorrectPassword: 'Ви ввели не правильный логин или пароль',
    incorrectEmail: 'Не правильный электронный адрес',
    incorrectPasswordCharacters: 'Пароль не должен содержать менее 6 символов',
    incorrectPasswordConfirm: 'Пароли не совпадют'
}
